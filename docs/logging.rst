=================================
This is the title of the document
=================================

:Author: Author
:Contact: mailaddress@example.com
:Date: 2019/12/13 00:00
       
Heading 1
--------------------

contents 1

Heading 1-1
~~~~~~~~~~~

contents 1-1

Heading 1-1-3
^^^^^^^^^^^^^

contents 1-1-3

Documenting Objects
---------------------------------------
documenting object

function expression writing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. py:function:: enumerate(sequence,[, start=0])

                 Return an iterator that yields tuples of an index and
                 an item of the *sequence*.(And so on.)
                 
.. function:: enumerate(sequence[, start=0])

              Return an iterator that yields tuples of an index and
                 an item of the *sequence*.(And so on.)
                 
call function                  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The  :py:func:`enumerate` function can be used for ...

              
AutoDoc
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
when documenting Python code,it is common to put a lot of
documentation in the source file,in documentation string.

- to document the function **io.open()**,reading its signature and
  docstring form the source file,you'd write this:
  
.. autofunction:: io.open

- You can also document whole classes or even modules
  automatically,using member options for the auto direct 

.. automodule:: io
   :members:

Intersphinx
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Many sphinx documents including the Python documentation are published
on the internet.When you want to make links to such documents from
your documentation,you can do it with *sphinx.ext.intersphinx*.

In order to use intersphinx,you need to activate it in *conf.py* by
putting the string *sphinx.ext.intersphinx* into the *extension* list
and set up the *intersphinx_mapping* config value.

For example,to link to `io.open()` in the python library manual,you
need to setup your `interphinx_mapping` like:

`intersphinx_mapping = {'python': ("https://docs.python.org/3",None)}`

and now,you can write a cross-reference like :py:func:`io.open()` , so
you will jump the ```io.open()``` explanation.
    
Doc test Blocks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 >>> var a = 1
 >>> var b = 2
 >>> print(a + b)
 
hello
~~~~~~~~~~~~~~~~
this is a link `a link`_.

.. _a link: https://baidu.com/

Field Lists
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

