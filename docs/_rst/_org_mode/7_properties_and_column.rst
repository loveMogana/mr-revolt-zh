======================
Properties and Columns
======================

    :Author: lyps

.. contents::

-**- mode:org; -**-

1 TODO Properties and Columns
-----------------------------

A property is a key-value pair associated with and entry.Properties
can be set so they are associated with a single entry,with every
entry in a tree,or with every entry in an Org file.

There are two main applications for properties in Org mode.First
property  are like tags,but with a value,Image maintaining a file
where you document bugs and plan releases for a piece of software
. Instead of using tags like ``release_1`` , ``release_2``, you can use a
property, say ``Release``, that in different subtrees has different
values,such as ``1.0`` ,that in different subtrees has different
values,such as ``1.0`` or ``2.0``. Second, you can use properties to
implement(very basic) database capabilities  [1]_  in an Org
buffer.Imagine keeping track of your music CDs,where properties
could be things such as the album,artist,date of release , members
of tracks ,and so on.

Properties can be conveniently edited and viewed in column view (see
Section 7.5 Column view).

1.1 Property Syntax
~~~~~~~~~~~~~~~~~~~

Properties are key-value pairs.when they are associated with a
single entry or with  a tree they need to inserted to a special
drawer with the name ``PROPERTIES`` ,which has to be located right
below a headline,and its planning line when applicable.Each
property is specified on a single line,with the key - surrounded by
colon -- first ,and the value after it.

key are case-insensitive.Here is an example:

`Case insenstive property <~/org/revolt/source/_org/_org_demo/_customize_property>`_

Depending on the value of ``org-use-property-inheritance``, a property
set this way is associated either with a single entry, or with the
sub-tree defined by the entry.

You may define the allowed values for a particular property ``'Xyz'`` by
setting a property ``'Xyz_ALL'`` This special property is inherited ,so
if you set it in a level 1 entry,it applies to the entire tree.

when allowed values are defined,setting the corresponding property
becomes easier and is less prone to typing errors.For the example with
the CD collection ,  we can pre-define publishers and the number of
disks in a box like this:

`pre-defined properties <~/org/revolt/source/_org/_org_demo/_customize_property>`_

if you want to set properties that can be inherited by any entry in
a file,use a line like:

::

    .#+PROPERY: NDisks_ALL 1 2 3 4

if you want to add to the value of an existing property , append a ``+``
to the property name.The following results in the property ``'var'``
having the value ``'foo=1 bar =2 '``.

::

    #+PROPERTY: var foo = 1
    #+PROPERTY: var+ bar = 2

It is also possible to add to the values of inherited properties.The
following results in the ``'Genres'`` property having the  ``'Classic Baroque'`` under the ``'Goldberg Variations'`` subtree.

`One Property has more values <~/org/revolt/source/_org/_org_demo/_customize_property>`_

Note that a property can only have one entry per drawer.

Property values set with the global variable ``org-global-properties``
can be inherited by all entries in all org files.

1.2 Special properties
~~~~~~~~~~~~~~~~~~~~~~

Special properties provide an alternative access method to Org mode
features,like the TODO state or the priority of an entry,discussed
in the previous chapters.This interface exists so that you can
include these states in a column view, or to use them in
queries.The following property names are special and should not be
used as keys in the  properties drawer:

1.3 Property Searches
~~~~~~~~~~~~~~~~~~~~~

- ``C-c / p``

property searches

1.4 Property Inheritance
~~~~~~~~~~~~~~~~~~~~~~~~

The outline structure of Org documents lends itself to an
inheritance model of properties: if the parent in a tree has a
certain property,the children can inherit this property.Org mode
does not turn this on by default,because it can slow down property
searches significantly and is often not needed.

However,if you find inheritance useful,you can turn it on by
setting the variable ``org-use-property-inheritance``.It may be set to
``t`` to make all properties inherited from the parents, to a list of
properties that should be inherited,or to a regular expression that
matches inherited properties.If a properties has the value ``nil``,this
is interpreted as an explicit un-define of the property,so that
inheritance search stops at this value and return ``nil``.

Org mode has a few properties fort which inheritance is hard-coded,
at least for the special applications for which they are used:

- ``COULMNS``

- ``CATEGORY``

- ``ARCHIVE``

- ``LOGGING``

1.5 Column View
~~~~~~~~~~~~~~~

A great way to view and edit properties in an outline tree is
``column view``. In column view,each outline node is turned into a
table row.Columns in this table provide access to properties of the
entries. Org mode implements columns by overlaying a tabular
structure over the headline of each item.While the headlines have
been turned into a table row,you can still change the visibility of
the outline tree.For example,you get a compact table by switching
to "content" view - S-TAB S-TAB, or simply c while column view is
active -- but you can still open,read,and edit the entry below each
headline.Or, you can switch to column view after executing a sparse
tree command and in this way get a table only for the selected
items. Column view also works in agenda buffers,where queries have
collected selected items,possibly from a number of files.

1.5.1 Defining columns
^^^^^^^^^^^^^^^^^^^^^^

Setting up a column view first requires defining the columns,This
is done by defining a column format line.

1.5.2 Scope of column definitions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To define a column format for an entire file,use a line like:

::

    #+COLUMN: %25ITEM %TAGS %PRIORITY %TODO

To specify a format that only applies to a specific tree,add a
``'column'`` property to the top node of that tree,for example:

`Set a Columns view <~/org/revolt/source/_org/_org_demo/_customize_property>`_

If a ``'COLUMN'`` property is present in an entry,it defines columns
for the entry itself, and for the entire subtree below it. Since
the column definition is part of the hierarchical structure of
the document,you can define columns on level 1 that are general
enough for all sublevel,and more specific columns further
down,when you edit a deeper part of the tree.

1.5.3 column attributes
^^^^^^^^^^^^^^^^^^^^^^^

A column definition sets the attributes of a column. The general
definition looks like this:

::

    %[WIDTH]PROPERTY[(TITLE)][{SUMMARY-TYPES}]

WIDTH
    An integer specifying the width of the column.If
    omitted,the width is determined automatically.

PROPERTY
    The property that should be edited in this
    column.Special properties representing meta data are
    allowed here as well.

TITLE
    The header text for the column.If omitted,the property
    named is used.

SUMMARY-TYPE
    The Summary type.If specified,the column values
    for parent nodes are computed from the children.

    supported summary types are:

    - 


.. [1] features
